var turno = true;
var puntajePlayer1 = 0
var puntajePlayer2 = 0
var empates = 0;
var vsPc = true;
var ultimaPosPC = 0;
var tiempoEspera = 750;




function marcarCasillero(num){

    if(celdaOcupada(num))
        return
    
    if(turno){
        document.getElementById("casilla"+num).innerHTML = "X"
        document.getElementsByClassName("celda"+num)[0].style.backgroundColor = "green";
        if(recorrerHorizontal("X")||recorrerDiagonalDerecha("X")||recorrerDiagonalIzquierda("X")
        ||recorrerVertical("X")){
            puntajePlayer1++;
            mostrarPuntaje();
            vaciarTablero();            
        }; 
    }
    if(!turno){
        document.getElementById("casilla"+num).innerHTML = "O"
        document.getElementsByClassName("celda"+num)[0].style.backgroundColor = "red";
       if(recorrerHorizontal("O")||recorrerDiagonalDerecha("O")||recorrerDiagonalIzquierda("O")
          ||recorrerVertical("O")){
            puntajePlayer2++;
            mostrarPuntaje(); 
            vaciarTablero();          
        }; 
    }
    
    
    if(vsPc){
        turno = !turno
        deshabilitarTablero()
        mostrarTurno() 
        setTimeout(movimientoPc,tiempoEspera);
    
    }
    else {
        turno = !turno;

        mostrarTurno()
        if(hayMovimiento()===false){
            vaciarTablero();
            empates++;
            document.getElementById("empates").innerHTML = empates;
            /* empate**/
        }    
    
}
    

}

function mostrarTurno(){
    mostrarPuntaje()

    if(turno){
        document.getElementById("turno").style.backgroundColor = "green"
        document.getElementById("turno").innerHTML = "Turno de X"
    }

    if(!turno){
        document.getElementById("turno").style.backgroundColor = "red"
        document.getElementById("turno").innerHTML = "Turno de O"
    }
}

function mostrarPuntaje(){
    document.getElementById("player1").innerHTML = puntajePlayer1;
    document.getElementById("player2").innerHTML = puntajePlayer2;
    document.getElementById("empates").innerHTML = empates;
}


function celdaOcupada(num) {
    let celda = document.getElementById("casilla"+num).innerHTML;
    if(celda.length>0)
        return true
    
    return false
}

function vaciarTablero(){

    for(let i = 1; i<=9;i++){
        document.getElementById("casilla"+i).innerHTML="";
        document.getElementsByClassName("celda"+i)[0].style.backgroundColor = "#252440"
    }


}
function hayMovimiento(){
    for(let i = 1; i<=9;i++){
        if(celdaOcupada(i)===false){
            return true;}
    }

    return false
}

function deshabilitarTablero(){

    for(let index=1;index<=9;index++){
        document.getElementsByClassName('celda'+index)[0].style.pointerEvents = 'none';
    }
}

function habilitarTablero(){

    for(let index=1;index<=9;index++){
        document.getElementsByClassName('celda'+index)[0].style.pointerEvents = 'auto';
    }
}



function recorrerDiagonalDerecha(valor){
    let acumuladorBool = true;

    for(let i=1;i<=9;i+=4){
        let val = document.getElementById("casilla"+i).innerHTML;

        acumuladorBool = acumuladorBool && val===valor
    }
    return acumuladorBool;
    }


function recorrerDiagonalIzquierda(valor){
    let acumuladorBool = true;

    for(let i=7;i>=3;i-=2){
        let val = document.getElementById("casilla"+i).innerHTML;
        acumuladorBool = acumuladorBool && val===valor
    }

    return acumuladorBool;

}

function recorrerVertical(valor){
    let acumuladorBool = true;
   
     for(let i =1;i<=3;i++){
        for(let j=i;j<=(i+6);j+=3){
            let val = document.getElementById("casilla"+j).innerHTML;
            acumuladorBool = acumuladorBool && val===valor;

        }
        if(acumuladorBool) return acumuladorBool;
    
        acumuladorBool = true
    }

    return false;
}   

function recorrerHorizontal(valor){
    let acumuladorBool = true;

    for(let i = 1;i<=7;i+=3){
        acumuladorBool = true;
        for(j=i;j<=i+2;j++){
            let val = document.getElementById("casilla"+j).innerHTML;
            acumuladorBool = acumuladorBool &&  val===valor;
        }
        if(acumuladorBool) return true

        acumuladorBool = true;
    }

    return false

}

function reiniciarPartida(){
    turno = true;
    mostrarTurno()
    puntajePlayer1=0;
    puntajePlayer2=0;
    empates = 0
    vaciarTablero();
    mostrarPuntaje();


}

function movimientoPc(){
    decisionMovPc()
    turno = !turno
    mostrarTurno()
    if(hayMovimiento()===false){
        vaciarTablero();
        empates++;
        document.getElementById("empates").innerHTML = empates;
        /* empate**/
    } 
    habilitarTablero()


    }


/*LOGICA PC */

function activarPC(){
    vsPc = true;
    
    reiniciarPartida()
}
function desactivarPC(){
    vsPc = false;
    reiniciarPartida()
}


function mover0(index){
    document.getElementById("casilla"+index).innerHTML="O";
    document.getElementsByClassName("celda"+index)[0].style.backgroundColor = "#ff0000"
    if(recorrerHorizontal("O")||recorrerDiagonalDerecha("O")||recorrerDiagonalIzquierda("O")
      ||recorrerVertical("O")){
            puntajePlayer2++;
            mostrarPuntaje(); 
            vaciarTablero();  
          }

}

function buscarGanarHorizontal(valor){
    let acumuladorBool = true;
    let ultimoIndice=0;

    for(let i = 1;i<=7;i+=3){
        acumuladorBool = true;
        for(j=i;j<=i+2;j++){
            let val = document.getElementById("casilla"+j).innerHTML
            acumuladorBool = acumuladorBool &&  (val===valor||val==="");
            if(val==="")
                ultimoIndice=j;
           // console.log(j+" "+acumuladorBool);
        }
        if(acumuladorBool) return ultimoIndice

        acumuladorBool = true;
    }

    return 0

}

function movimientoPcVertical(valor){
    let acumuladorBool = true;
    let ultimaPosPC = 0;

     for(let i =1;i<=3;i++){
        for(let j=i;j<=(i+6);j+=3){
            let val = document.getElementById("casilla"+j).innerHTML;
            acumuladorBool = acumuladorBool && (val===valor||val==="");
            console.log(j+" "+val+" "+acumuladorBool);

            if(val==="") 
                ultimaPosPC = j;
            

        }
        if(acumuladorBool) return ultimaPosPC;
    
        acumuladorBool = true
    }

    return 0;
}   


function movimientoPcDiagonalIzquierda(valor){
    let acumuladorBool = true;
    let ultimaPosPC = 0;

    for(let i=7;i>=3;i-=2){
        let val = document.getElementById("casilla"+i).innerHTML;
        acumuladorBool = acumuladorBool && (val===valor||val==="");
        
        if(val==="") ultimaPosPC=i;
    }
    

    if(acumuladorBool)
        return ultimaPosPC

    return 0;

}


function moverPcDiagonalDerecha(valor){
    let acumuladorBool = true;
    let ultimaPosPC = 0;

    for(let i=1;i<=9;i+=4){
        let val = document.getElementById("casilla"+i).innerHTML;

        acumuladorBool = acumuladorBool && (val===valor||val==="");
        console.log(i );
        if(val==="") ultimaPosPC = i ;
    }
    if(acumuladorBool)
        return ultimaPosPC;

    return 0;
}

function decisionMovPc(){
    let arrMov = [];
    let moverDiagonalDer = moverPcDiagonalDerecha("O");    
    let moverDiagonalIZQ = movimientoPcDiagonalIzquierda("O");
    let movHorizontal =buscarGanarHorizontal("O")
    let movVertical = movimientoPcVertical("O")
   
   if(moverDiagonalIZQ) arrMov.push(moverDiagonalIZQ);
    
   if (movVertical)     arrMov.push(movVertical);

   if(movHorizontal)    arrMov.push(movHorizontal);
    
   if(moverDiagonalDer) arrMov.push(moverDiagonalDer);
   
   if(arrMov.length>0) {     
      let indiceMov = Math.floor(Math.random() * arrMov.length);
      let movimientoPC = arrMov[indiceMov];
      mover0(movimientoPC);
   }
   else{
    for(let i =1;i<=9;i++){
        let val = document.getElementById("casilla"+i).innerHTML;
        if(val===""){
            ultimaPosPC = i;
            mover0(i)
            return;
        }
                }    
       }

                            }
